<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for outputting the Udemy course format.
 *
 * @package format_udemy
 * @copyright 2021 lmsace
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 namespace format_eduemy\output;


defined('MOODLE_INTERNAL') || die();


// use format_topics\output\renderer;
use core_courseformat\output\section_renderer;
use html_writer;
use context_course;
use completion_info;
use moodle_page;

/**
 * Basic renderer for topics format.
 *
 * @copyright 2012 Dan Poltawski
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class renderer extends section_renderer {

    /**
     * Constructor method, calls the parent constructor.
     *
     * @param moodle_page $page
     * @param string $target one of rendering target constants
     */
    public function __construct(moodle_page $page, $target) {
        parent::__construct($page, $target);

        // Since format_topics_renderer::section_edit_control_items() only displays the 'Highlight' control
        // when editing mode is on we need to be sure that the link 'Turn editing mode on' is available for a user
        // who does not have any other managing capability.
        $page->set_other_editing_capability('moodle/course:setcurrentsection');
    }

    /**
     * Generate the section title, wraps it in a link to the section page if page is to be displayed on a separate page.
     *
     * @param section_info|stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @return string HTML to output.
     */
    public function section_title($section, $course) {
        return $this->render(course_get_format($course)->inplace_editable_render_section_name($section));
    }

    /**
     * Generate the section title to be displayed on the section page, without a link.
     *
     * @param section_info|stdClass $section The course_section entry from DB
     * @param int|stdClass $course The course entry from DB
     * @return string HTML to output.
     */
    public function section_title_without_link($section, $course) {
        return $this->render(course_get_format($course)->inplace_editable_render_section_name($section, false));
    }

    public function get_initial_course_module($course, $modinfo) {
        $cms = array_keys($modinfo->get_cms());
        if (!empty($cms)) {
            return reset($cms);
        }
    }

    public function render_content($widget) {
        global $DB;

        $course = $this->page->course;
        $format = course_get_format($course);
        $modinfo = $format->get_modinfo();
        $data = $widget->export_for_template($this);

        $mod = optional_param('module', null, PARAM_TEXT);
        $currentmodule = optional_param('cmid', 0, PARAM_INT);
        if ($mod !== null) {
            if (!$DB->get_record('course_module', ['id' => $currentmodule])) {
                $currentmodule = '';
            }
        }

        if (!isset($currentmodule) || empty($currentmodule)) {
            $currentmodule = $this->get_initial_course_module($course, $modinfo);
            if (empty($currentmodule)) {
                return '';
            }
        }

        $cm = $modinfo->get_cm($currentmodule);
        // $modinfo = $widget->format->()->get_modinfo();
        $data->viewport_activityurl = $cm->url;
        $data->courseoverviewsection = $this->render_courseoverview($course, $data);
    
        echo $this->render_from_template('format_eduemy/content', $data);
    }

    public function render_title($widget) {
        $data = $widget->export_for_template($this);
        return $this->render_from_template('format_eduemy/title', $data);
    }

    public function render_courseoverview($course, $data) {
        $overview = new \format_eduemy\overview($course, $data);
        return $overview->render();
    }
}