<?php


namespace format_eduemy\output\courseformat\content\cm;

use cm_info;
use core_courseformat\base as course_format;
use renderable;
use section_info;
use stdClass;
use templatable;
use core_text;
use core_courseformat\output\local\content\cm\title as cm_title;



class title extends cm_title {

    /** @var course_format the course format */
    protected $format;

    /** @var section_info the section object */
    private $section;

    /** @var cm_info the course module instance */
    protected $mod;

    /** @var array optional display options */
    protected $displayoptions;

    /**
     * Constructor.
     *
     * @param course_format $format the course format
     * @param section_info $section the section info
     * @param cm_info $mod the course module ionfo
     * @param array $displayoptions optional extra display options
     */
    public function __construct(course_format $format, section_info $section, cm_info $mod, array $displayoptions = []) {
        $this->format = $format;
        $this->section = $section;
        $this->mod = $mod;
        $this->displayoptions = $displayoptions;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param \renderer_base $output typically, the renderer that's calling this function
     * @return stdClass data context for a mustache template
     */
    public function export_for_template(\renderer_base $output): stdClass {

        $format = $this->format;
        $mod = $this->mod;
        $displayoptions = $this->displayoptions;

        if (!$mod->is_visible_on_course_page() || !$mod->url) {
            // Nothing to be displayed to the user.
            return new stdClass();
        }

        // Usually classes are loaded in the main cm output. However when the user uses the inplace editor.
        // the cmname output does not calculate the css classes.
        if (!isset($displayoptions['linkclasses']) || !isset($displayoptions['textclasses'])) {
            $cmclass = $format->get_output_classname('content\\cm');
            $cmoutput = new $cmclass(
                $format,
                $this->section,
                $mod,
                $displayoptions
            );
            $displayoptions['linkclasses'] = $cmoutput->get_link_classes();
            $displayoptions['textclasses'] = $cmoutput->get_text_classes();
        }

        $url = new \moodle_url('/course/view.php', ['id' => $mod->course, 'cmid' => $mod->id]);
        $data = (object)[
            'url' => $url,
            'instancename' => $mod->get_formatted_name(),
            'uservisible' => $mod->uservisible,
            'icon' => $mod->get_icon_url(),
            'linkclasses' => $displayoptions['linkclasses'],
            'textclasses' => $displayoptions['textclasses'],
        ];

        // File type after name, for alphabetic lists (screen reader).
        if (strpos(
            core_text::strtolower($data->instancename),
            core_text::strtolower($mod->modfullname)
        ) === false) {
            $data->altname = get_accesshide(' ' . $mod->modfullname);
        }

        // Get on-click attribute value if specified and decode the onclick - it
        // has already been encoded for display (puke).
        $data->onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);

        return $data;
    }

}