<?php

namespace format_eduemy;

use moodle_url;
use stdClass;
use core_course_list_element;
use context_course;
use core_user;
use user_picture;

class overview {

    protected $course;

    function __construct($course, $coursecontent) {
        global $DB;

        if ($course instanceof stdClass) {
            $course = new core_course_list_element($course);
        }
        if ($coursecontent) {
            $this->sections = $coursecontent->sections;
            $this->initialsection = $coursecontent->initialsection;
        }
        // $modinfo = $course->get_modinfo();
        $this->studentrole = $DB->get_record('role', array('shortname' => 'student'));
        $this->teacherrole = $DB->get_record('role', array('shortname' => 'editingteacher'));

        $format = course_get_format($course);
        $this->course = $course;
        $this->formatoptions = $format->get_format_options();
        $this->context = context_course::instance($course->id);
        $this->activities = get_array_of_activities($course->id);
        $this->coursestudents = get_role_users($this->studentrole->id , $this->context);
        $this->teachers = get_role_users($this->teacherrole->id , $this->context);
        
        $this->modinfo = $format->get_modinfo();
    }

    function course_instructor_information() {
        global $PAGE, $DB, $CFG, $OUTPUT;

        $data = [];
        foreach ($this->teachers as $key => $val) {
            $list = [];
            $user = core_user::get_user($val->id);
            $userpicture = new user_picture($user);
            $userpicture->size = true;
            $list['img'] = $userpicture->get_url($PAGE);
            $list['description'] = format_string($user->description, FORMAT_HTML);
            $list['username'] = fullname($user);
            $list['url'] = new moodle_url('/user/profile.php', ['id' => $val->id]);
            $list['teacherid'] = $val->id;
            $instructorinfo = $this->instructor_courses($val->id);
            $courses = $instructorinfo['coursesids'];
            $list['students'] = $instructorinfo['students'];
            $list['courses'] = count($courses);
            if (file_exists($CFG->dirroot.'/blocks/courserate/block_courserate.php')) {
                if (!empty($courses)) {
                    list($dbsql,$dbparam) = $DB->get_in_or_equal($courses,SQL_PARAMS_NAMED);
                    $sql = "SELECT * FROM {block_course_rateing} WHERE course $dbsql";
                    $records = $DB->get_records_sql($sql, $dbparam);
                    if (!empty($records)) {
                        // split no of rate users.
                        $tot_user = count($records);
                        $split_rate = array_column($records,'rating');
                        $split_review = array_column($records,'review');
                        $bring_count_review = count(array_filter($split_review));
                        $bring_tot_rate = array_sum($split_rate);
                        $avg_rate= $bring_tot_rate / $tot_user;
                        $list['avg_rate'] = number_format($avg_rate, 1);
                        $list['bring_count_review'] = $bring_count_review;
                    }
                }
            }
            $data[] = $list;
        }
        return $data;
    }


    public function instructor_courses($teacher) {
        global $DB;
        $courseids = [];
        $students = 0;
        $courses = enrol_get_users_courses($teacher);
        if (!empty($courses)) {
            foreach ($courses as $course) {
                $coursecontext = context_course::instance($course->id);
                $roles = get_role_users($this->studentrole->id, $coursecontext);
                $userroles = get_user_roles($coursecontext, $teacher);
                $userrole = current($userroles);
                if ($userrole->shortname == 'teacher' || $userrole->shortname == 'coursecreator' || $userrole->shortname == 'editingteacher') {
                    $courseids[] = $course->id;
                    $students += count($roles);
                }
            }
        }

        return ['coursesids' => $courseids, 'students'=> $students];
    }

    function course_stats($course) {

        global $CFG;

        $stats = [];
        $enrols = enrol_get_course_users($this->course->id);
        $list = get_array_of_activities($this->course->id);
        $mods = array_unique(array_column($list, 'mod'));
        $courses = get_course($this->course->id);
        $meta = \core_course\local\factory\content_item_service_factory::get_content_item_service();
        $siteadmins = explode(',', $CFG->siteadmins);
        if (!empty($siteadmins) && isset($siteadmins[0])) {
            $adminuser = \core_user::get_user($siteadmins[0]);
            $content_item_service = \core_course\local\factory\content_item_service_factory::get_content_item_service();
            $meta = $content_item_service->get_all_content_items($adminuser);

            $resources= array();
            $activities = array();
            foreach ($meta as $key=> $value) {
                foreach ($mods as $mod) {
                    if ($value->name == $mod) {
                        if ($value->archetype == 1) {
                            $resources[] = $value->name;
                        } elseif ($value->archetype == 0) {
                            $num = $value;
                            $activities[] = $value->name;
                        }
                        $stats[$mod] = (in_array($mod, $stats)) ? $stats[$mod] + 1 : 1;
                    }
                }
            }

            $stats['enrols'] = !empty($enrols) ? count($enrols) : '0';
            $stats['activities'] = count($activities);
            $stats['resources'] = count($resources);
        }
        return $stats;
    }

    public function course_currency_symbol($code) {

        $codes = array(
            'AUD' => '&#36;',
            'BRL' => '&#82;&#36;',
            'CAD' => '&#36;',
            'CHF' => '&#67;&#72;&#70;',
            'CZK' => '&#75;&#269;',
            'DKK' => '&#107;&#114;',
            'EUR' => '&#8364;',
            'GBP' => '&#163;',
            'HKD' => '&#36;',
            'HUF' => '&#70;&#116;',
            'ILS' => '&#8362;',
            'JPY' => '&#165;',
            'MXN' => '&#36;',
            'MYR' => '&#82;&#77;',
            'NOK' => '&#107;&#114;',
            'NZD' => '&#36;',
            'PHP' => '&#8369;',
            'PLN' => '&#122;&#322;',
            'RUB' => '&#8381;',
            'SEK' => '&#107;&#114;',
            'SGD' => '&#36;',
            'THB' => '&#3647;',
            'TRY' => '',
            'TWD' => '&#78;&#84;&#36;',
            'USD' => '&#36;',
        );

        return $codes[$code];
    }

    function get_course_content() {
        global $OUTPUT, $CFG, $USER;
        $course = $this->course;
        $categoryinfo = \core_course_category::get($course->category);
        if (!empty($course)) :
            $noimgurl = '';
            $imgurl = '';
            $data['id'] = $course->id;
            $data['url'] = new moodle_url('/course/info.php', array('id' => $course->id ));
            $data['enrolurl'] = new moodle_url('/enrol/index.php', array('id' => $course->id));
            $data['name'] = $course->get_formatted_name();
            $data['coursename'] =  $course->get_formatted_name();
            $data['category'] = $categoryinfo->get_formatted_name();
            $data['courseprogress'] = $course_progress = \core_completion\progress::get_course_progress_percentage($course, $USER->id);
            $data['courseurl'] = new moodle_url('/course/view.php', array('id' => $course->id) );
            $data['categoryurl'] = new moodle_url('/course/index.php', array('categoryid' => $course->category));
            $data['summary'] = $course->summary;
            
            $data['startdate'] = !empty($course->startdate) ? userdate($course->startdate, '%d %B, %Y', '', false) : 0;
            $data['enddate'] = !empty($course->enddate) ? userdate($course->enddate, '%d %B, %Y', '', false) : 0;
            $data['summarytext'] = trim($course->summary);
            $data['user_enrol'] = is_enrolled($this->context, $USER->id, '', true);
            $data['coursestudents'] = count($this->coursestudents);
            $data['teachers'] = count($this->teachers);
            $data['teacher_list'] = $this->course_instructor_information($this->teachers, $course->id);
            $data['categoryid'] = $course->category;
            $enrolplugin = enrol_get_instances($course->id, true);
            $data['payment_methods'] = array();
            foreach ($enrolplugin as $key => $value) {
                $pix_icon = $OUTPUT->pix_icon('icon', $value->name, 'enrol_'.$value->enrol);
                $data['payment_methods'][] = array('method' => $value->name, 'id' => $value->id, 'img' => $pix_icon);
                if ($value->enrol == 'paypal') {
                    $cost = $value->cost;
                    $currency = $value->currency;
                }
            }
            if (isset($currency) && isset($cost)) {
                $currency = $this->course_currency_symbol($currency);
                $data['cost'] = $currency . ' ' . $cost;
            }
            $data['lectures'] = count($this->activities);
            foreach ($course->get_course_overviewfiles() as $file) {
                $isimage = $file->is_valid_image();
                $courseimgurl = file_encode_url("$CFG->wwwroot/pluginfile.php",
                    '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                    $file->get_filearea(). $file->get_filepath(). $file->get_filename());

                if($courseimgurl) {
                    $data['courseimg'] = true;
                }

                if ($isimage) {
                    $data['imgurl'] = $courseimgurl;
                } else {
                    if ($courseimgurl) {
                        $data['videourl'] = $courseimgurl;
                    }
                }
            }
            if (empty($data['imgurl'])) {
                $data['imgurl'] = $noimgurl;
            }
            $stats = $this->course_stats($course->id);
            $formatoptions = (array) $this->formatoptions;
            // Course overview tabs.
            
       /*     $tabmodules = json_decode($customdata->tabmodules) ?? [];
             $tabs = [];
            foreach ($tabmodules as $cmid) {
                $moddata = get_coursemodule_from_id('', $cmid);
                $tabs[] = [
                    'id' => $moddata->id,
                    'name' => $moddata->name,
                    'url' => new moodle_url('/mod/'.$moddata->modname.'/view.php', ['id' => $cmid]),
                ];
            }
            $data['tabs'] = $tabs; */
            $data = array_merge($data, $stats, $formatoptions);

        endif;
        return $data;
    }

    /**
     * get section info data
     *
     * @param stdClass $course The course entry from DB
     */
    function get_sectionmodule($course) {
        global $DB, $CFG, $USER;

        $t = get_fast_modinfo($course);

        $sectionmod = $sectionname = $sectionsummary = $sectioncount = array();
        $totalhours = 0;
        if ($sections = $DB->get_records('course_sections', array('course' => $course->id))) {

            foreach ($sections as $key => $value) {
                $sequence = '';
                $sectionname[$value->id] = $value->name;
                if ($value->sequence) {
                    $sequence = explode(',', $value->sequence);
                }
                $sectionmod[$value->id] = isset($sequence) ? $sequence : '';

            }
        }

        $mod = get_course_mods($course->id);
        $customdata = (object) $this->formatoptions;
        // print_r($course);
        $previewmod = ($customdata->previewmod) ? json_decode($customdata->previewmod) : [];
        foreach ($mod as $key => $value) {

            if (isset($sectionmod[$value->section]) ) {

                if ($value->visible == 1) {
                    $modvalues['name'] = $t->get_cm($value->id)->name;
                    $modvalues['moduleid'] = $value->module;
                    $modvalues['modname'] = $value->modname;
                    $modvalues['image'] = $CFG->wwwroot.'/mod/'.$value->modname.'/pix/icon.png';
                    $modvalues['url'] = new moodle_url('/mod/'.$value->modname.'/view.php', ['id' => $value->id]);
                    $modvalues['cmid'] = $value->id;
                    if (in_array($value->id, $previewmod)) {
                        $modvalues['preview'] = true;
                    } else {
                        $modvalues['preview'] = false;
                    }
                    if ( isset($value->completion_time) && !empty($value->completion_time) ) {
                        $totalhours += (int) $value->completion_time;
                    }
                    $modules[$value->id] = $modvalues;

                }
            }
        }

        $newsection['mods'] = array();
        $incre = 1;
        $numactivity = 0;
        foreach ($sectionmod as $key => $secs) {

            $details = array();
            if (!empty($secs)) {
                $cnt = 0;
                $activitycompletion = 0;
                foreach ($secs as $k) {
                $list = [] ;
                if (isset($modules[$k])) {
                        $list = $modules[$k];
                        if ($this->is_coursemodule_completed($modules[$k]['cmid'])) {
                            $activitycompletion++;
                        }
                        $numactivity++;
                        $list['numactivity'] = $numactivity;
                    }
                    if (!empty($list)) {
                        $details[] = $list;
                    }
                $cnt++;
                }

                $sectiontitle = $sectionname[$key];
                $accordstate = false;
                if ($incre == 1) {
                    $accordstate = true;
                }
                if (empty($sectiontitle)) {

                    if($incre == 1) {
                        $sectiontitle = get_string('general','format_udemy');
                    } else {
                        $sectiontitle = get_string('sectionstr','format_udemy', $incre);
                    }
                }

                $moddetails = array(
                    'name' => $sectiontitle,
                    'id' => $key,
                    'totcount' => $cnt,
                    'modcompletioncount' => $activitycompletion,
                    'sectionnum' => $incre,
                    'accordstate' => $accordstate,
                    'details' => $details
                    );

                array_push($newsection['mods'], $moddetails);
                $incre++;
            }
        }
        return $newsection;
    }

    public function render() {
        global $OUTPUT;
        $data = $this->get_course_content($this->course->id);
        // $blockshtml = $OUTPUT->blocks('tabs-modules');
        // $PAGE->blocks->add_region('side-pre');

        // $blockshtml = $bm->region_has_content('side-pre', $OUTPUT);
        // $hasblocks = strpos($blockshtml, 'data-block=') !== false;
        $data['sections'] = $this->sections;
        $data['initialsection'] = $this->initialsection;
        $data['courseview'] = true;
        // $data['tabsmodulesblocks'] = $blockshtml;
        // $data['hasblocks'] = $hasblocks;
        $data['output'] = $OUTPUT;
        return $OUTPUT->render_from_template('format_eduemy/listcourseview', $data);
    }

    /**
     * Check module completed or not
     *
     * @param int course module id
     * @return bool status
     */
    function is_coursemodule_completed($cmid) {
        global $DB, $USER;

        $condition = array('coursemoduleid' => $cmid, 'completionstate' => 1, 'userid' => $USER->id);
        if ($DB->record_exists('course_modules_completion', $condition)) {
            return true;
        }

        return false;
    }

    public function info_page() {
        global $DB, $OUTPUT, $PAGE;
        $context = context_course::instance($this->course->id);
        $PAGE->requires->js_call_amd('format_udemy/previewpopup', 'init', array($context->id));
        // $data = $this->format_udemy_course_details($this->course);
        $data = $this->get_course_content();
        $rating = new \aceaddon_courserate\rating($this->course->id);

        $data['totsections'] = $DB->count_records('course_sections', array('course' => $this->course->id));
        $sectioninfo = $this->get_sectionmodule($this->course);
        $data['sectioninfo'] = $sectioninfo;

        // $data = array_merge_recursive($data, $content);
        $data['navbar'] = $OUTPUT->navbar();
        $data['courseinfo'] = true;
        $data['courserate'] = $rating->courserate_widget();
        $data['ratingwidget'] = $rating->rating_widget();
        $data['reviewwidget'] = $rating->review_widget();

        // print_object($data);
        // $data = array_merge($data, $coursedata);
        return $OUTPUT->render_from_template('format_eduemy/listcourseinfo', $data);
    }

    function format_udemy_course_details($course) {
        global $USER, $DB;
    
        $data = [];
        $coursecontent = [];
        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $context = context_course::instance($course->id);
    
        // Title with completion help icon.
        $completioninfo = new \completion_info($course);
        $couseinfo = new core_course_list_element($course);
        $categoryinfo = \core_course_category::get($course->category);
        $data['coursename'] =  $couseinfo->get_formatted_name();
        $data['category'] = $categoryinfo->get_formatted_name();
        $data['courseprogress'] = $course_progress = \core_completion\progress::get_course_progress_percentage($course, $USER->id);
        $data['courseurl'] = new moodle_url('/course/view.php', array('id' => $course->id) );
        $data['categoryurl'] = new moodle_url('/course/index.php', array('categoryid' => $course->category));
        $data['summary'] = $course->summary;
        $numsections = course_get_format($course)->get_last_section_number();
        $sectioninfo = format_udemy_course_get_sectionmodule($couseinfo);
        $data['sectioninfo'] = $sectioninfo;
        $ratinginfo = format_udemy_course_rating_info($course->id);
        $data = array_merge($data, $ratinginfo);
        return $data;
    }
}