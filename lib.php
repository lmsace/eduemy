<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

// require_once($CFG->dirroot. '/course/format/lib.php');
require_once($CFG->dirroot. '/course/format/topics/lib.php');


use core\output\inplace_editable;

/**
 * Main class for the Topics course format.
 *
 * @package    format_udemy
 * @copyright  2021 lmsace
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class format_eduemy extends format_topics {

    public $editorvalue = ['aboutcoursedesc', 'certificatedesc', 'featuredesc', 'coursedescinfo', 'whatlearn', 'requirements'];

    /**
     * The URL to use for the specified course (with section).
     *
     * @param int|stdClass $section Section object from database or just field course_sections.section
     *     if omitted the course view page is returned
     * @param array $options options for view URL. At the moment core uses:
     *     'navigation' (bool) if true and section has no separate page, the function returns null
     *     'sr' (int) used by multipage formats to specify to which section to return
     * @return null|moodle_url
     */
    public function get_view_url($section, $options = []) {
        global $CFG;
        $course = $this->get_course();
        $url = new moodle_url('/course/view.php', ['id' => $course->id]);

        $sr = null;
        if (array_key_exists('sr', $options)) {
            $sr = $options['sr'];
        }
        if (is_object($section)) {
            $sectionno = $section->section;
        } else {
            $sectionno = $section;
        }
        if ($sectionno !== null) {
            if ($sr !== null) {
                if ($sr) {
                    $usercoursedisplay = COURSE_DISPLAY_MULTIPAGE;
                    $sectionno = $sr;
                } else {
                    $usercoursedisplay = COURSE_DISPLAY_SINGLEPAGE;
                }
            } else {
                $usercoursedisplay = 0;
            }
            if ($sectionno != 0 && $usercoursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                $url->param('section', $sectionno);
            } else {
                if (empty($CFG->linkcoursesections) && !empty($options['navigation'])) {
                    return null;
                }
                $url->set_anchor('section-'.$sectionno);
            }
        }
        return $url;
    }

    public function get_course() {
        $course = parent::get_course();
        // $editorvalue = ['aboutcoursedesc', 'certificatedesc', 'featuredesc', 'coursedescinfo', 'whatlearn', 'requirements'];
        foreach ($this->editorvalue as $value) {
            if (isset($course->{$value}) && is_string($course->{$value})) {
                $course->$value = [
                    'format' => ($course->{$value.'_format'}) ?? '',
                    'text' => $course->{$value} ?? ''
                ];
            }
        }
        if (isset($course->previewmod) && is_string($course->previewmod)) {
            $course->previewmod = json_decode($course->previewmod);
        }
        return $course;
    }

    /**
     * Updates format options for a course.
     *
     * In case if course format was changed to 'topics', we try to copy options
     * 'coursedisplay' and 'hiddensections' from the previous format.
     *
     * @param stdClass|array $data return value from {@link moodleform::get_data()} or array with data
     * @param stdClass $oldcourse if this function is called from {@link update_course()}
     *     this object contains information about the course before update
     * @return bool whether there were any changes to the options values
     */
    public function update_course_format_options($data, $oldcourse = null) {
        $data = (array)$data;

        $editorvalue = ['aboutcoursedesc', 'certificatedesc', 'featuredesc', 'coursedescinfo', 'whatlearn', 'requirements'];
        foreach ($this->editorvalue as $value) {
            $data[$value.'_format'] = (($data[$value]['format']) ?? '');
            $data[$value] = (($data[$value]['text']) ?? '');
        }
        if (isset($data['previewmod'])) {
            $data['previewmod'] = json_encode($data['previewmod']);
        }
        if ($oldcourse !== null) {
            $oldcourse = (array)$oldcourse;
            $options = $this->course_format_options();
            foreach ($options as $key => $unused) {
                if (!array_key_exists($key, $data)) {
                    if (array_key_exists($key, $oldcourse)) {
                        $data[$key] = $oldcourse[$key];
                    }
                }
            }
        }
        return $this->update_format_options($data);
    }

     /**
     * Definitions of the additional options that this course format uses for course.
     *
     * Topics format uses the following options:
     * - coursedisplay
     * - hiddensections
     *
     * @param bool $foreditform
     * @return array of options
     */
    public function course_format_options($foreditform = false) {
        static $courseformatoptions = false;
        if ($courseformatoptions === false) {
            $courseconfig = get_config('moodlecourse');
            
            $courseformatoptions = [
                'hiddensections' => [
                    'default' => $courseconfig->hiddensections,
                    'type' => PARAM_INT,
                ],
                'coursedisplay' => [
                    'default' => $courseconfig->coursedisplay,
                    'type' => PARAM_INT,
                ],
                'overviewhead' => [ 'default' => ''],
                'skillevel' => ['default' => '', 'type' => PARAM_TEXT],
                'languages' => ['default' => '', 'type' => PARAM_TEXT],
                'captions' => ['default' => '', 'type' => PARAM_TEXT],
                'duration' => ['default' => '', 'type' => PARAM_FLOAT],
                'articles' => ['default' => '', 'type' => PARAM_INT],
                'resources' => ['default' => '', 'type' => PARAM_INT],
                'fulltimeaccess' => ['default' => '', 'type' => PARAM_INT],
                'accesstvandmobile' => ['default' => '', 'type' => PARAM_INT],
                'certificatecompletion' => ['default' => '', 'type' => PARAM_INT],
                'displayreviews' => ['default' => '', 'type' => PARAM_INT],
                'previewmod' => ['default' => '', 'type' => PARAM_TEXT],
                // 'tabmodules' => ['default' => '', 'type' => PARAM_TEXT],
                'courseinfo' => ['default' => ''],
            ];

            foreach ($this->editorvalue as $value) {
                $courseformatoptions[$value] = [ 'default' => ['text' => '', 'format' => FORMAT_HTML], 'type' => PARAM_RAW ];
                $courseformatoptions[$value.'_format'] = [ 'default' => FORMAT_HTML, 'type' => PARAM_INT ];
            }
        }
        if ($foreditform ) {
            // $mods = format_eduemy_get_course_modinfo($this->courseid);
            global $PAGE;
            $modinfo = $this->get_modinfo();
            $sections = $modinfo->get_section_info_all();
            $output = $PAGE->get_renderer('format_eduemy');
            $modules = [];
            foreach ($sections as $num => $section) {
                if (!isset($modinfo->sections[$section->section])) {
                    continue;
                }
                foreach ($modinfo->sections[$section->section] as $modnumber) {
                    $mod = $modinfo->cms[$modnumber];
                    $modules[$mod->id] = $mod->name;
                }
                /* $cmlist = new \core_courseformat\output\local\content\section\cmlist($this, $section);
                $activities = $cmlist->export_for_template($output);
                foreach ($activities as $key => $activity) {
                    foreach ($activity as $cmitem) {
                        print_r($cmitem);
                    }
                } */
            }

            // print_r($modules);

            // print_object($activities);
            $courseformatoptionsedit = [
                'hiddensections' => [
                    'label' => new lang_string('hiddensections'),
                    // 'help' => 'hiddensections',
                    // 'help_component' => 'moodle',
                    'element_type' => 'select',
                    'element_attributes' => [
                        [
                            0 => new lang_string('hiddensectionscollapsed'),
                            1 => new lang_string('hiddensectionsinvisible')
                        ],
                    ],
                ],

                'coursedisplay' => [
                    'label' => new lang_string('coursedisplay'),
                    'element_type' => 'select',
                    'element_attributes' => [
                        [
                            COURSE_DISPLAY_SINGLEPAGE => new lang_string('coursedisplay_single'),
                            COURSE_DISPLAY_MULTIPAGE => new lang_string('coursedisplay_multi'),
                        ],
                    ],
                    // 'help' => 'coursedisplay',
                    // 'help_component' => 'moodle',
                ],

                'overviewhead' => [
                    'label' => new lang_string('overviewhead', 'format_eduemy'),
                    'element_type' => 'header',
                ],
                'duration' => [
                    'label' => new lang_string('duration', 'format_eduemy'),
                    'element_type' => 'text',
                    // 'help' => 'duration',
                    // 'help_component' => 'format_eduemy',
                ],

                'skillevel' =>  [
                    'label' => new lang_string('skillevel', 'format_eduemy'),
                    'element_type' => 'text',
                    // 'help' => 'skillevel',
                    // 'help_component' => 'format_eduemy',
                ],
                'languages' =>  [
                    'label' => new lang_string('languages', 'format_eduemy'),
                    'element_type' => 'text',
                    // 'help' => 'languages',
                    // 'help_component' => 'format_eduemy',
                ],
                'captions' =>  [
                    'label' => new lang_string('captions', 'format_eduemy'),
                    'element_type' => 'text',
                    // 'help' => 'captions',
                    // 'help_component' => 'format_eduemy',
                ],
                'duration' =>  [
                    'label' => new lang_string('duration', 'format_eduemy'),
                    'element_type' => 'text',
                    // 'help' => 'duration',
                    // 'help_component' => 'format_eduemy',
                ],
                'articles' =>  [
                    'label' => new lang_string('articles', 'format_eduemy'),
                    'element_type' => 'advcheckbox',
                    // 'help' => 'articles',
                    // 'help_component' => 'format_eduemy',
                ],
                'resources' =>  [
                    'label' => new lang_string('resources', 'format_eduemy'),
                    'element_type' => 'advcheckbox',
                    // 'help' => 'resources',
                    // 'help_component' => 'format_eduemy',
                ],
                'fulltimeaccess' =>  [
                    'label' => new lang_string('fulltimeaccess', 'format_eduemy'),
                    'element_type' => 'advcheckbox',
                    // 'help' => 'fulltimeaccess',
                    // 'help_component' => 'format_eduemy',
                ],
                'accesstvandmobile' => [
                    'label' => new lang_string('accesstvandmobile', 'format_eduemy'),
                    'element_type' => 'advcheckbox',
                    // 'help' => 'accesstvandmobile',
                    // 'help_component' => 'format_eduemy',
                ],
                'certificatecompletion' => [
                    'label' => new lang_string('certificatecompletion', 'format_eduemy'),
                    'element_type' => 'advcheckbox',
                    // 'help' => 'certificatecompletion',
                    // 'help_component' => 'format_eduemy',
                ],
                'displayreviews' => [
                    'label' => new lang_string('displayreviews', 'format_eduemy'),
                    'element_type' => 'advcheckbox',
                    // 'help' => 'displayreviews',
                    // 'help_component' => 'format_eduemy',
                ],
                'previewmod' => [
                    'label' => new lang_string('previewmod', 'format_eduemy'),
                    'element_type' => 'autocomplete',
                    'element_attributes' => [$modules, ['multiple' => 'true']],
                    // 'help' => 'previewmod',
                    // 'help_component' => 'format_eduemy',
                ],
                /* 'tabmodules' => [
                    'label' => new lang_string('tabmodules', 'format_eduemy'),
                    'element_type' => 'autocomplete',
                    'element_attributes' => [$modules],
                    // 'help' => 'tabmodules',
                    // 'help_component' => 'format_eduemy',
                ], */
                'courseinfo' => [
                    'label' => new lang_string('courseinfo', 'format_eduemy'),
                    'element_type' => 'header',
                    // 'help' => 'courseinfo',
                    // 'help_component' => 'format_eduemy',
                ],
            ];

            foreach($this->editorvalue as $value) {
                $courseformatoptionsedit[$value] = [
                    'label' => new lang_string($value, 'format_eduemy'),
                    'element_type' => 'editor',
                    // 'help' => $value,
                    // 'help_component' => 'format_eduemy',
                ];

                $courseformatoptionsedit[$value.'_format'] = [
                    'label' => 'hidden',
                    'element_type' => 'hidden',
                ];
            }
            /*
                'certificatedesc' => [
                    'label' => new lang_string('certificatedesc', 'format_eduemy'),
                    'element_type' => 'editor',
                    // 'help' => 'certificatedesc',
                    // 'help_component' => 'format_eduemy',
                ],
                'featuredesc' => [
                    'label' => new lang_string('featuredesc', 'format_eduemy'),
                    'element_type' => 'editor',
                    // 'help' => 'featuredesc',
                    // 'help_component' => 'format_eduemy',
                ],
                'coursedescinfo' => [
                    'label' => new lang_string('coursedescinfo', 'format_eduemy'),
                    'element_type' => 'editor',
                    // 'help' => 'coursedescinfo',
                    // 'help_component' => 'format_eduemy',
                ],
                'courseinfo' => [
                    'label' => new lang_string('courseinfo', 'format_eduemy'),
                    'element_type' => 'header',
                    // 'help' => 'courseinfo',
                    // 'help_component' => 'format_eduemy',
                ],
                'whatlearn' => [
                    'label' => new lang_string('whatlearn', 'format_eduemy'),
                    'element_type' => 'editor',
                    // 'help' => 'whatlearn',
                    // 'help_component' => 'format_eduemy',
                ],
                'requirements' => [
                    'label' => new lang_string('requirements', 'format_eduemy'),
                    'element_type' => 'editor',
                    // 'help' => 'requirements',
                    // 'help_component' => 'format_eduemy',
                ], 

            ];*/
            $courseformatoptions = array_merge_recursive($courseformatoptions, $courseformatoptionsedit);
        }

        return $courseformatoptions;

    }


    public function create_edit_form_elements(&$mform, $forsection = false) {
        $elements = array();
        if ($forsection) {
            $options = $this->section_format_options(true);
        } else {
            $options = $this->course_format_options(true);
        }
        foreach ($options as $optionname => $option) {
            if (!isset($option['element_type'])) {
                $option['element_type'] = 'text';
            }
            $args = array($option['element_type'], $optionname, $option['label']);
            if (!empty($option['element_attributes'])) {
                $args = array_merge($args, $option['element_attributes']);
            }
            if ($option['element_type'] == 'header') {
                $elements[] = $mform->addElement('header', $optionname, $option['label'] );
            } else {
                $elements[] = call_user_func_array(array($mform, 'addElement'), $args);
            }
            if (isset($option['help'])) {
                $helpcomponent = 'format_'. $this->get_format();
                if (isset($option['help_component'])) {
                    $helpcomponent = $option['help_component'];
                }
                $mform->addHelpButton($optionname, $option['help'], $helpcomponent);
            }
            if (isset($option['type'])) {
                $mform->setType($optionname, $option['type']);
            }
            if (isset($option['default']) && !array_key_exists($optionname, $mform->_defaultValues)) {
                // Set defaults for the elements in the form.
                // Since we call this method after set_data() make sure that we don't override what was already set.
                $mform->setDefault($optionname, $option['default']);
            }
        }
        return $elements;
    }

    public function info_page($course) {
        global $DB, $OUTPUT, $PAGE;
        $context = context_course::instance($course->id);
        $PAGE->requires->js_call_amd('format_udemy/previewpopup', 'init', array($context->id));
        $data = format_udemy_course_details($course);
        $data['totsections'] = $DB->count_records('course_sections', array('course' => $course->id));
        $content = format_udemy_get_course_content($course->id);
        $data = array_merge_recursive($data, $content);
        $data['navbar'] = $OUTPUT->navbar();
        $data['courseinfo'] = true;
        $coursedata = (array) format_udemy_custom_settings_value($course->id);
        $data = array_merge($data, $coursedata);
        return $OUTPUT->render_from_template('format_eduemy/listcourseinfo', $data);
    }


}

function format_eduemy_coursemodule_standard_elements($instance, &$mform) {
    $mform->addElement('header', 'duration-heading', 'Duration');
    $mform->addElement('duration', 'duration', 'Duration');
    $mform->setType('duration', PARAM_INT);
}

function format_eduemy_get_course_modinfo($courseid) {
    // global $DB;

    // $course = $DB->get_record('course', array('id' => $courseid));
    // $data = [];
    // $t = get_fast_modinfo($course);
    // $mods = get_course_mods($course->id);
    // if (!empty($mods)) {
    //     foreach ($mods as $key => $value) {
    //         $modulename = $t->get_cm($value->id)->name;
    //         $cmid = $value->id;
    //         $data[$cmid] = $modulename;
    //     }
    // }
    // return $data;
}